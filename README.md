# Basic-monolith-node-js



# This is basically to avoid executing pipeline on FEATURE + COMMIT

If it is not a "main" branch AND! the trigger is not a merge request (both has to be true)
when: never = execute never
when: always =  otherwise execute it always

"&&" is used to chain commands together, such that the next command is run if and only if the preceding
command exited without errors (or, more accurately, exits with a return code of 0).

  workflow:
    rules:
      - if: $CI_COMMIT_BRANCH != "main" && $CI_PIPELINE_SOURCE != "merge_request_event"      
        when: never
      - when: always


# It executes on "feature" + merge request. 
# It doesn't execute on "feature" + commit.
# It executes on "main" + commit.

Because we have this : 

        build_image:
        only:
            - main
        stage: build
        script: 
            - echo "Building the docker image..."
            - echo "Tagging the docker image" 

On "feature" + merge request it will only run "test" jobs. 

When "approved" it will run all jobs.

# By default, GitLab uses one of its shared runners to run your CI/CD jobs.
# Docker machine executors are used for them.
# It uses ruby:2.5 image.


You can override it with image: attribute (Global or job level).


        run_some_tests:
          image: node:17-alpine3.14
          stage: test
          before_script:
            - cd app
            - npm install
          script:
            - npm test
          artifacts:
            when: always
            paths:
              - app/junit.xml
            reports:
              junit: app/junit.xml

# ARTIFACTS : path, dotenv.

We don’t have an overview of the test executions inside the GitLab UI.
In our package.json file we have an option that generates reports for our tests in junit.xml format.
However we don’t see any reports in test tab. 
Also in the list-view of the pipelines we have no artifacts for the test reports.


For that we have an attribute called artifacts.
There are a couple of formats and we are going to use junit format and specify the generated junit file that jest gives us.
We want those reports to be uploaded as artifacts not only when the tests pass but also when tests fail.
And for that we need to add when condition when: always.

You want to make your downloadable test reports browsable with a folder structure so we can add an additional attribute here called paths . It will make sense if you have multiple test reports that you want organize and not have them all in one place.

        before_script:
          - export PACKAGE_JSON_VERSION=$(cat app/package.json | jq -r .version)
          - export VERSION=$PACKAGE_JSON_VERSION.$CI_PIPELINE_IID  # . to append
          - echo "VERSION=$VERSION" > build.env # Main part 
          - echo "MY_ENV=value" >> build.env # only for example
        script:
          - docker build -t $IMAGE_NAME:$VERSION .
        artifacts:
          reports:
            dotenv: build.env


First of all instead of saving the environment variable from a .txt file , we need the file to be with .env extension.
And we need to actually save the environment variable key:value pairs not just the value, not just one thing but it has to have a specific format. 
So we need to save <environment variable> = <whatever_value>  > build.env file.
And you can save multiple such variables into the build.env file but in this case we would need to append each value >> inside the file and not > override it .
And in the artifacts attribute instead of this generic paths attribute we are going to reference build.env file.

        push_image_dotenv:
          stage: some_dotenv
          needs:
            - build_image_dotenv
          dependencies:
            - build_image_dotenv
          tags:
            - ec2
            - shell
            - remote
          before_script:
          # - export VERSION=$(cat version-file.txt) # Not needed anymore
            - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
          script:
            - docker push $IMAGE_NAME:$VERSION

And now in the jobs where we actually need that value, this environment variable that we are saving here will be automatically available as an environment variable.
So GitLab will do this part export VERSION for you, which means you don’t even have to read it out from a file and then set it and export it - this will be done for you.
So this is automatically exported and made available for you.


        build_image_dotenv:
          stage: some_dotenv
          image: docker:24.0.5
          services:
            - docker:24.0.5-dind
          before_script:
            - apk add --no-cache jq # Install jq
            - export PACKAGE_JSON_VERSION=$(cat app/package.json | jq -r .version)
            - export VERSION=$((${PACKAGE_JSON_VERSION//./-}+${CI_PIPELINE_IID}+1))
            - echo $VERSION
            - echo "VERSION=$VERSION" > build.env
            - echo "MY_ENV=value" >> build.env # only for example
          script:
            - docker build -t $IMAGE_NAME:$VERSION .
            - docker save $IMAGE_NAME:$VERSION | gzip > image-2.tar.gz
          artifacts:
            reports:
              dotenv: build.env
            paths:
              - image-2.tar.gz # Saving image between jobs


        push_image_dotenv:
          stage: some_dotenv
          image: docker:24.0.5
          services:
            - docker:24.0.5-dind
          needs:
            - build_image_dotenv
          dependencies:
            - build_image_dotenv
          before_script:
          # - export VERSION=$(cat version-file.txt) # Not needed anymore
            - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
            - docker load -i image-2.tar.gz
          script:
            - echo $VERSION
            - docker push $IMAGE_NAME:$VERSION

In build_image_dotenv we are saving docker image into a zipped file,
then we are creating an artifact for that zipped file.

In push_image_dotenv we are loading this image from the artifact to current job environment 
and then we push it to the container registry.
